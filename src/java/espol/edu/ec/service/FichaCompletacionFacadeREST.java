/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.service;

import espol.edu.ec.model.FichaCompletacion;
import espol.edu.ec.model.Niveles;
import espol.edu.ec.model.Usuarios;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Stateless
@Path("fichacompletacion")
public class FichaCompletacionFacadeREST extends AbstractFacade<FichaCompletacion> {

    @EJB
    private UsuariosFacadeREST usuariosFacadeREST;
    @EJB
    private NivelesFacadeREST nivelesFacadeREST;
    
    
    @PersistenceContext(unitName = "FundamentosPlayPU")
    private EntityManager em;

    public FichaCompletacionFacadeREST() {
        super(FichaCompletacion.class);
    }

    @POST
    @Override
    @Consumes({ MediaType.APPLICATION_JSON})
    public void create(FichaCompletacion entity) {
        super.create(entity);
    }

    @GET
    @Path("find/{id}")
    @Produces({ MediaType.APPLICATION_JSON})
    public FichaCompletacion find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<FichaCompletacion> findAll() {
        return super.findAll();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("updateMedallas/{userHash}/{nivel}/{dificultad}")
    public FichaCompletacion updateFicha(@PathParam("nivel") int nivel, @PathParam("userHash") String userHash, @PathParam("dificultad") int dificultad ) {
        
        FichaCompletacion  ficha =getFicha(nivel, userHash);
        if(null == ficha)
            return null;
     
        switch(dificultad){
            case 0:
               ficha.setMedallaPrincipiante(true);
               break;
            case 1:
               ficha.setMedallaIntermedio(true);
               break;
            case 2:
               ficha.setMedallaExperto(true);
               break;
                
        }
        edit(ficha);
               
        return ficha;
    }
    
     
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("getMedallas/{userHash}/{nivel}")
    public FichaCompletacion getFicha(@PathParam("nivel") int nivel, @PathParam("userHash") String userHash) {
        
        Usuarios user = usuariosFacadeREST.find(userHash);
        Niveles level = nivelesFacadeREST.find(nivel);
        if(user == null || level == null)
            return null;
        
        
        Query q = em.createQuery("SELECT f FROM FichaCompletacion f WHERE f.nivel = :nivel AND f.userId = :user");
        q.setParameter("nivel",level );
        q.setParameter("user", user);
        q.setHint("javax.persistence.cache.storeMode", "REFRESH");   // Esto toma los datos directos de la base y no desde la cache 
        FichaCompletacion  ficha =(FichaCompletacion) q.getSingleResult();
        return ficha;
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("updateFichaData/{userHash}/{nivel}/{pacertadas}/{pfalladas}/{iacertadas}/{ifalladas}/{eacertadas}/{efalladas}")
    public FichaCompletacion updateQuestionData(@PathParam("nivel") int nivel, @PathParam("userHash") String userHash,
                                                @PathParam("pacertadas") int pacertadas, @PathParam("pfalladas") int pfalladas,
                                                @PathParam("iacertadas") int iacertadas, @PathParam("ifalladas") int ifalladas,
                                                @PathParam("eacertadas") int eacertadas, @PathParam("efalladas") int efalladas) {
        
        FichaCompletacion ficha =  getFicha(nivel, userHash);
        ficha.setPrincipianteAcertadas(ficha.getPrincipianteAcertadas() + pacertadas);
        ficha.setPrincipanteFalladas(ficha.getPrincipanteFalladas() + pfalladas);
        ficha.setIntermedioAcertadas(ficha.getIntermedioAcertadas() + iacertadas);
        ficha.setIntermedioFalladas(ficha.getIntermedioFalladas() + ifalladas);
        ficha.setExpertoAcertadas(ficha.getExpertoAcertadas() + eacertadas);
        ficha.setExpertoFalladas(ficha.getExpertoFalladas() + efalladas);
        return ficha;
    }
}
