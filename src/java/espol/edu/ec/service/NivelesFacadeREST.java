/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.service;

import espol.edu.ec.model.Niveles;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Stateless
@Path("niveles")
public class NivelesFacadeREST extends AbstractFacade<Niveles> {

    @PersistenceContext(unitName = "FundamentosPlayPU")
    private EntityManager em;

    public NivelesFacadeREST() {
        super(Niveles.class);
    }

    @POST
    @Override
    @Consumes({ MediaType.APPLICATION_JSON})
    public void create(Niveles entity) {
        super.create(entity);
    }
   

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Niveles> findAll() {
        return super.findAll();
    }

   
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("/updateLevelData/{level}/{pregTotales}/{pregErroneas}/{pregCorrectas}")
    @Produces({MediaType.APPLICATION_JSON})
    public Niveles updateLevelData(@PathParam("level") Integer level, @PathParam("pregTotales") int pregTotales, 
            @PathParam("pregErroneas") int pregErroneas, @PathParam("pregCorrectas") int pregCorrectas ) {
        Niveles nivel = find(level);
        nivel.setPreguntasTotales(nivel.getPreguntasTotales() + pregTotales);
        nivel.setPreguntasAcertadas(nivel.getPreguntasAcertadas()+ pregCorrectas);
        nivel.setPreguntasErroneas(nivel.getPreguntasErroneas()+ pregErroneas);
        return nivel;
    }
    
}
