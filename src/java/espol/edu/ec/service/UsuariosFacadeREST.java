/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.service;

import espol.edu.ec.model.FichaCompletacion;
import espol.edu.ec.model.Niveles;
import espol.edu.ec.model.Usuarios;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Stateless
@Path("usuarios")
public class UsuariosFacadeREST extends AbstractFacade<Usuarios> {

    @EJB
    private FichaCompletacionFacadeREST fichaCompletacionFacadeREST;

    @EJB
    private NivelesFacadeREST nivelesFacadeREST;

    
    
    


    @PersistenceContext(unitName = "FundamentosPlayPU")
    private EntityManager em;

    public UsuariosFacadeREST() {
        super(Usuarios.class);
    }

    
    /** 
     * Busca un usuario a partir de su userHash.
     * @param userHash El userHash del usuario
     * @return el usuario encontrado.
     */
    @GET
    @Path("find/{userHash}")
    @Produces({ MediaType.APPLICATION_JSON})
    public Usuarios find(@PathParam("userHash") String userHash) {
        Query query = em.createNamedQuery("Usuarios.findByUserHash");
            query.setParameter("userHash", userHash);
            query.setHint("javax.persistence.cache.storeMode", "REFRESH");   // Esto toma los datos directos de la base y no desde la cache 
            Usuarios user = (Usuarios) query.getSingleResult();
            return user;
    }
    
    
    /** 
     * Busca un usuario a partir de su username.
     * @param username El username del usuario
     * @return el usuario encontrado.
     */
    @GET
    @Path("findByUsername/{username}")
    @Produces({ MediaType.APPLICATION_JSON})
    public Usuarios findByUsername(@PathParam("username") String userName ) {
       try{
           
            Query query = em.createNamedQuery("Usuarios.findByUsername");
            query.setParameter("username", userName);
            query.setHint("javax.persistence.cache.storeMode", "REFRESH");   // Esto toma los datos directos de la base y no desde la cache 
            Usuarios user = (Usuarios) query.getSingleResult();
            return user;
       }catch(Exception e){
           return null;
       }
    }
    
    /** 
     * Busca un usuario a partir de su id.
     * @param id El id del usuario
     * @return el usuario encontrado.
     */
    @GET
    @Path("findById/{id}")
    @Produces({ MediaType.APPLICATION_JSON})
    public Usuarios findById(@PathParam("id") Integer id ) {
        return super.find(id);
    }
    
    @GET
    @Override
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Usuarios> findAll() {
        return super.findAll();
    }





    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    /** 
     * Actualiza los datos del usuario luego de cada ronda.
     * @param hashId El userHash del usuario
     * @param pregTotales El núumero de preguntas contestadas.
     * @param pregErroneas El número de preguntas contestadas erróneamente.
     * @param pregCorrectas El número de preguntas contestadas correctamente.
     * @return el usuario con los datos actualizados.
     */
    @GET
    @Path("/updateUserData/{userHash}/{pregTotales}/{pregErroneas}/{pregCorrectas}")
    @Produces({MediaType.APPLICATION_JSON})
    public Usuarios updateUser(@PathParam("userHash") String hashId, @PathParam("pregTotales") int pregTotales, 
            @PathParam("pregErroneas") int pregErroneas, @PathParam("pregCorrectas") int pregCorrectas ) {
        Usuarios user = find(hashId);
        if(user != null){
            user.setPreguntasTotales(user.getPreguntas_totales() + pregTotales);
            user.setPreguntasAcertadas(user.getPreguntas_correctas() + pregCorrectas);
            user.setPreguntasEquivocadas(user.getPreguntas_erroneas() + pregErroneas);
            edit(user);
        }
        return user;
    }
    
    /** 
     * Actualiza el ultimo nivel visitado por el usuario.
     * @param userHash El userHash del usuario
     * @param level El último nivel visitado por el usuario.
     * @return el usuario con los datos actualizados.
     */
    @GET
    @Path("/updateVisitedLevel/{userHash}/{level}")
    @Produces({MediaType.APPLICATION_JSON})
    public Usuarios updateVisitedLevel(@PathParam("userHash") String userHash, @PathParam("level") int level ) {
        Usuarios user = find(userHash);
        if(user != null){
            user.setUltimoNivelVisitado(level);
            edit(user);
        }
        return user;
    }
    
    /** 
     * Incrementa en uno el nivel alcanzado por el usuario.
     * @param userHash El userHash del usuario
     * @return el usuario con los datos actualizados.
     */
    @GET
    @Path("/updateLevel/{userHash}")
    @Produces({MediaType.APPLICATION_JSON})
    public Usuarios updateLevel(@PathParam("userHash") String userHash ) {
        Usuarios user = find(userHash);
        if(user != null){
            user.setMaximoNivelAlcanzado(user.getMaximo_nivel() + 1);
            edit(user);
        }
        return user;
    }
    
    /** 
     * Comprueba las credenciales del usuario para hacer un inicio de sesion.
     * @param username El username del usuario.
     * @param pass La contraseña del usuario.
     * @return el userHash del usuario si las credenciales son válidas, y null si son inválidas o esta inactivo.
     */
    @GET
    @Path("/login/{username}/{password}")
    @Produces({MediaType.TEXT_PLAIN})
    public String login(@PathParam("username") String username, @PathParam("password") String pass){
        Usuarios user = findByUsername(username);
        if(user == null) return null;
        if(!pass.equals(user.getContrasena())) return null;
        if(!user.getIsActive()){
            return null;
        }
        return user.getUserHash();
            
    }
    
    
 
    @GET
    @Path("/create/{username}/{password}/{name}")
    @Produces({MediaType.APPLICATION_JSON})
    public Usuarios createUser(@PathParam("username") String username, @PathParam("password") String pass,  @PathParam("name") String name){
    
        Usuarios user = new Usuarios(username ,name, sha256(pass), 1, 1, 0, 0, 0, -1);
        create(user);
        user = findByUsername(username);
        user.setUserHash(sha256(String.valueOf(user.getUserId())));
        edit(user);
        for(Niveles nivel : nivelesFacadeREST.findAll()){
            FichaCompletacion ficha = new FichaCompletacion(user,nivel);
            fichaCompletacionFacadeREST.create(ficha);
        }
        return user;
            
    }
    
   
    
    /** 
     * Actualiza el avatar el usuario.
     * @param userHash El username del usuario.
     * @param pos El índice del avatar.
     * @return el usuario con los datos actualizados.
     */
    @GET
    @Path("/updateAvatar/{userHash}/{pos}")
    @Produces({MediaType.APPLICATION_JSON})
    public Usuarios updateAvatar(@PathParam("userHash") String userHash, @PathParam("pos") int pos){
        Usuarios user = find(userHash);
        if(user == null) return null;
        user.setAvatar(pos);
        return user;
            
    }
    

    
    
    /** 
     * Codifica un String con el algoritmo sha1 256.
     * @param base String a codificar.
     * @return el String codificado en sha1 256.
     */
    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(UnsupportedEncodingException | NoSuchAlgorithmException ex){
           throw new RuntimeException(ex);
        }
    }


    
}
