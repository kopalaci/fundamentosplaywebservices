/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.service;

import espol.edu.ec.model.FichaCompletacion;
import espol.edu.ec.model.LeaderBoardEntity;
import espol.edu.ec.model.Leaderboard;
import espol.edu.ec.model.Niveles;
import espol.edu.ec.model.Usuarios;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Stateless
@Path("leaderboard")
public class LeaderboardFacadeREST extends AbstractFacade<Leaderboard> {
    
    @EJB
    private UsuariosFacadeREST usuariosFacadeREST;
    @EJB
    private NivelesFacadeREST nivelesFacadeREST;
    @EJB
    private FichaCompletacionFacadeREST fichaCompletacionFacadeREST;

    @PersistenceContext(unitName = "FundamentosPlayPU")
    private EntityManager em;

    public LeaderboardFacadeREST() {
        super(Leaderboard.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    @GET
    @Override
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Leaderboard> findAll() {
        return super.findAll();
    }

    @Override
    public void edit(Leaderboard entity) {
        super.edit(entity);
    }

    
    @GET
    @Path("/getLeaders/{userHash}")
    @Produces( MediaType.APPLICATION_JSON)
    public LeaderBoardEntity getLeaders( @PathParam("userHash") String userHash) {
        List<Boolean> chall = new LinkedList<>();
        List<Float> times  = new LinkedList<>();
        List<String> names  = new LinkedList<>();
        List<Integer> avatares  = new LinkedList<>();
        List<Integer> dias  = new LinkedList<>();
        FichaCompletacion ficha;
        for(Niveles nivel : nivelesFacadeREST.findAll()){
            ficha = fichaCompletacionFacadeREST.getFicha(nivel.getNivel(), userHash);
            if(ficha.getMedalla_experto()){
                chall.add(Boolean.TRUE);
            }
            else
                chall.add(Boolean.FALSE);
        }
        Usuarios user ;
        for(Leaderboard leader : findAll()){
            user = usuariosFacadeREST.findById(leader.getUserId());
            times.add(Float.valueOf(leader.getTiempo()));
            names.add(user.getUsername());
            avatares.add(user.getAvatar());
            dias.add(calculateDays(leader.getStart_day()));
        }
        return new LeaderBoardEntity(chall, times, names , avatares,dias);
    }
    
    @GET
    @Path("/updatePosition/{userHash}/{level}/{time}")
    @Produces( MediaType.APPLICATION_JSON)
    public Leaderboard updatePosition(@PathParam("level") Integer level, @PathParam("userHash") String userHash, @PathParam("time") String time) throws ParseException {
        Usuarios user = usuariosFacadeREST.find(userHash);
        if(user !=null && level>0 && level<10){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Leaderboard leader = new Leaderboard(time, level, user,dateFormat.parse(LocalDate.now().toString()));
            edit(leader);
            return leader;
        }
        
        return null;
    }
    
    private int calculateDays(Date fechaInicial){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date fechaFinal=dateFormat.parse(LocalDate.now().toString());
            int dias=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/86400000);
            if(dias<1) return 1;
            return dias;
            
        } catch (ParseException ex) {
            Logger.getLogger(LeaderboardFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
}
