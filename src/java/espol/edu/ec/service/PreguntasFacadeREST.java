/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.service;

import espol.edu.ec.model.AllRoundData;
import espol.edu.ec.model.Niveles;
import espol.edu.ec.model.Preguntas;
import espol.edu.ec.model.Usuarios;
import espol.edu.ec.wrapper.AllRoundDataWrapper;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Stateless
@Path("preguntas")
public class PreguntasFacadeREST extends AbstractFacade<Preguntas> {

    @EJB
    private NivelesFacadeREST nivelesFacadeREST;

    @PersistenceContext(unitName = "FundamentosPlayPU")
    private EntityManager em;

    public PreguntasFacadeREST() {
        super(Preguntas.class);
    }

    @POST
    @Override
    @Consumes({  MediaType.APPLICATION_JSON})
    public void create(Preguntas entity) {
        super.create(entity);
    }


    @GET
    @Override
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Preguntas> findAll() {
        return super.findAll();
    }


    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    /** 
     * Toma aleatoriamente 10 preguntas en base a los parametros enviados.
     * @param dificultad La dificultad de las preguntas.
     * @param nivel El nivel de las preguntas.
     * @return una lista de 10 preguntas aleatorias.
     */
    public List<Preguntas> randomRound(Integer dificultad ,Niveles nivel) {
        int cantidad = 10;
        boolean condition = true;
        Query q = em.createQuery("SELECT p FROM Preguntas p WHERE p.dificultad = :dificultad AND p.nivel = :nivel AND p.state ='Aprobada'");
       
        q.setParameter("dificultad", dificultad);
        q.setParameter("nivel", nivel);
        q.setHint("javax.persistence.cache.storeMode", "REFRESH");   // Esto toma los datos directos de la base y no desde la cache 
        List<Preguntas> preguntasRandom  = new LinkedList<>();
        List<Preguntas> preguntas  = q.getResultList();
        
        if(!preguntas.isEmpty()){
            while(condition){
                Collections.shuffle(preguntas);
                Preguntas pregunta = preguntas.get(0);
                if(!preguntasRandom.contains(pregunta)) {
                    Collections.shuffle((List)pregunta.getAnswers());
                    preguntasRandom.add(pregunta);
                }
                if(preguntasRandom.size() == cantidad){
                    condition = false;
                }
            }
        }
        return preguntasRandom ;
    }
    
    
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("getRonda/{nivel}/{dificultad}")
    public AllRoundDataWrapper ronda(@PathParam("nivel") Integer nivel, @PathParam("dificultad") Integer dificultad) {
        Niveles niveles = nivelesFacadeREST.find(nivel);
        List<Preguntas> preguntas = randomRound(dificultad, niveles);
        List<AllRoundData> allRoundDataList = new LinkedList<>();
        allRoundDataList.add(new AllRoundData(preguntas));
        return new AllRoundDataWrapper(allRoundDataList);
    }
    
    @GET
    @Path("/updateQuestionData/{idPregunta}/{correctas}/{fallidas}")
    @Produces({MediaType.APPLICATION_JSON})
    public Preguntas updateQuestionData(@PathParam("idPregunta") int idPregunta, @PathParam("correctas") int correctas, @PathParam("fallidas") int fallidas ) {
        Preguntas pregunta = find(idPregunta);
        pregunta.setVecesAcertada(pregunta.getVecesAcertada() + correctas);
        pregunta.setVecesFallada(pregunta.getVecesFallada() + fallidas);
        return pregunta;
    }
    
}
