/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.utils;

import org.python.util.PythonInterpreter;

/**
 *
 * @author Kevin Palacios
 */
public class Utils {
    
    public  String etiquetarTexto(String entrada){
        PythonInterpreter interpreter = new PythonInterpreter();

        // Set a variable with the content you want to work with
        interpreter.set("entrada", entrada);
        

        // Simple use Pygments as you would in Python
        interpreter.exec("from pygments import highlight\n" +
"from pygments.formatter import Formatter\n" +
"from pygments.lexers.python import PythonLexer\n" +
"from pygments.style import Style\n" +
"from pygments.token import Keyword, Name, Comment, String,  \\\n" +
"    Number, Operator\n" +
"\n" +
"\n" +
"\n" +
"class fundamentosPlayFormatter(Formatter):\n" +
"	def __init__(self, **options):\n" +
"		Formatter.__init__(self, **options)\n" +
"\n" +
"		self.styles = {}\n" +
"\n" +
"		for token, style in self.style:\n" +
"			start = end = ''\n" +
"			if style['color']:\n" +
"				start += '<#%s>' % style['color']\n" +
"				end = '</color>' + end\n" +
"			if style['bold']:\n" +
"				start += '<b>'\n" +
"				end = '</b>' + end\n" +
"			self.styles[token] = (start, end)\n" +
"	\n" +
"	def format(self, tokensource, outfile):\n" +
"\n" +
"		lastval = ''\n" +
"		lasttype = None\n" +
"\n" +
"\n" +
"		for ttype, value in tokensource:\n" +
"\n" +
"			while ttype not in self.styles:\n" +
"				ttype = ttype.parent\n" +
"			if ttype == lasttype:\n" +
"				lastval += value\n" +
"			else:\n" +
"				if lastval:\n" +
"					stylebegin, styleend = self.styles[lasttype]\n" +
"					outfile.write(stylebegin + lastval + styleend)\n" +
"				lastval = value\n" +
"				lasttype = ttype\n" +
"\n" +
"		if lastval:\n" +
"			stylebegin, styleend = self.styles[lasttype]\n" +
"			outfile.write(stylebegin + lastval + styleend)\n" +
"\n" +
"class FundamentosStyle(Style):\n" +
"    default_style = \"\" \n" +
"    styles = {\n" +
"        Comment:                '#d00000',\n" +
"        Keyword:                'bold #00007f',\n" +
"        Name.Function:          '#003ac8',\n" +
"        Name.Decorator:         '#CC00A3',\n" +
"        Name.Class:             '#00007f',\n" +
"        String:                 '#ff8000',\n" +
"        Number:                	'#007f3a',\n" +
"		Operator.Word: 			'bold #00007f',\n" +
"		Name.Builtin:			'bold #991111',				\n" +
"    }\n" +
"\n" +
"def tagCode(s):\n" +
"	s=s.replace(\"\\t\",\"\\\\t\")\n" +
"	s=s.replace(\"        \",\"\\\\t\")\n" +
"	s=(highlight(s, PythonLexer(),fundamentosPlayFormatter(style=FundamentosStyle))).replace(\"\\n\",\"\\\\n\")\n" +
"	return s\n" +
"\n" +
"resultado=(tagCode(entrada))\n" +
"\n" +
"");

        // Get the result that has been set in a variable
        String resultado = interpreter.get("resultado", String.class);
        return resultado;
   }
}
