/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
@Table(name = "respuestas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Respuestas.findAll", query = "SELECT r FROM Respuestas r")
    , @NamedQuery(name = "Respuestas.findById", query = "SELECT r FROM Respuestas r WHERE r.id = :id")
    , @NamedQuery(name = "Respuestas.findByText", query = "SELECT r FROM Respuestas r WHERE r.text = :text")
    , @NamedQuery(name = "Respuestas.findByIsCorrect", query = "SELECT r FROM Respuestas r WHERE r.isCorrect = :isCorrect")
    , @NamedQuery(name = "Respuestas.findByRetroalimentacion", query = "SELECT r FROM Respuestas r WHERE r.retroalimentacion = :retroalimentacion")})
public class Respuestas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "text")
    private String text;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_correct")
    private boolean isCorrect;
    @Size(max = 255)
    @Column(name = "retroalimentacion")
    private String retroalimentacion;
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Preguntas questionId;

    public Respuestas() {
    }

    public Respuestas(Integer id) {
        this.id = id;
    }

    public Respuestas(Integer id, String text, boolean isCorrect) {
        this.id = id;
        this.text = text;
        this.isCorrect = isCorrect;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getIdResp() {
        return id;
    }

    public void setIdResp(Integer id) {
        this.id = id;
    }

    public String getContenido() {
        return text;
    }

    public void setContenido(String text) {
        this.text = text;
    }

    public boolean getEsCorrecta() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }
    
    public String getRetroalimentacion() {
        return retroalimentacion;
    }

    public void setRetroalimentacion(String retroalimentacion) {
        this.retroalimentacion = retroalimentacion;
    }
    @XmlTransient
    @JsonIgnore
    public Preguntas getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Preguntas questionId) {
        this.questionId = questionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Respuestas)) {
            return false;
        }
        Respuestas other = (Respuestas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "espol.edu.ec.model.Respuestas[ id=" + id + " ]";
    }
    
}
