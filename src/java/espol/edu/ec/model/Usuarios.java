/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
@Table(name = "usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuarios.findAll", query = "SELECT u FROM Usuarios u")
    , @NamedQuery(name = "Usuarios.findByUltimoNivelVisitado", query = "SELECT u FROM Usuarios u WHERE u.ultimoNivelVisitado = :ultimoNivelVisitado")
    , @NamedQuery(name = "Usuarios.findById", query = "SELECT u FROM Usuarios u WHERE u.id = :id")
    , @NamedQuery(name = "Usuarios.findByPassword", query = "SELECT u FROM Usuarios u WHERE u.password = :password")
    , @NamedQuery(name = "Usuarios.findByLastLogin", query = "SELECT u FROM Usuarios u WHERE u.lastLogin = :lastLogin")
    , @NamedQuery(name = "Usuarios.findByIsSuperuser", query = "SELECT u FROM Usuarios u WHERE u.isSuperuser = :isSuperuser")
    , @NamedQuery(name = "Usuarios.findByUsername", query = "SELECT u FROM Usuarios u WHERE u.username = :username")
    , @NamedQuery(name = "Usuarios.findByNombre", query = "SELECT u FROM Usuarios u WHERE u.nombre = :nombre")
    , @NamedQuery(name = "Usuarios.findByApellido", query = "SELECT u FROM Usuarios u WHERE u.apellido = :apellido")
    , @NamedQuery(name = "Usuarios.findByEmail", query = "SELECT u FROM Usuarios u WHERE u.email = :email")
    , @NamedQuery(name = "Usuarios.findByIsStaff", query = "SELECT u FROM Usuarios u WHERE u.isStaff = :isStaff")
    , @NamedQuery(name = "Usuarios.findByIsActive", query = "SELECT u FROM Usuarios u WHERE u.isActive = :isActive")
    , @NamedQuery(name = "Usuarios.findByDateJoined", query = "SELECT u FROM Usuarios u WHERE u.dateJoined = :dateJoined")
    , @NamedQuery(name = "Usuarios.findByIsStudent", query = "SELECT u FROM Usuarios u WHERE u.isStudent = :isStudent")
    , @NamedQuery(name = "Usuarios.findByIsTeacher", query = "SELECT u FROM Usuarios u WHERE u.isTeacher = :isTeacher")
    , @NamedQuery(name = "Usuarios.findByIsAssistant", query = "SELECT u FROM Usuarios u WHERE u.isAssistant = :isAssistant")
    , @NamedQuery(name = "Usuarios.findByPhone", query = "SELECT u FROM Usuarios u WHERE u.phone = :phone")
    , @NamedQuery(name = "Usuarios.findByIsLogged", query = "SELECT u FROM Usuarios u WHERE u.isLogged = :isLogged")
    , @NamedQuery(name = "Usuarios.findByAvatar", query = "SELECT u FROM Usuarios u WHERE u.avatar = :avatar")
    , @NamedQuery(name = "Usuarios.findByMaximoNivel", query = "SELECT u FROM Usuarios u WHERE u.maximoNivel = :maximoNivel")
    , @NamedQuery(name = "Usuarios.findByPreguntasCorrectas", query = "SELECT u FROM Usuarios u WHERE u.preguntasCorrectas = :preguntasCorrectas")
    , @NamedQuery(name = "Usuarios.findByPreguntasErroneas", query = "SELECT u FROM Usuarios u WHERE u.preguntasErroneas = :preguntasErroneas")
    , @NamedQuery(name = "Usuarios.findByPreguntasTotales", query = "SELECT u FROM Usuarios u WHERE u.preguntasTotales = :preguntasTotales")
    , @NamedQuery(name = "Usuarios.findByUserHash", query = "SELECT u FROM Usuarios u WHERE u.userHash = :userHash")
    , @NamedQuery(name = "Usuarios.findByIsAdmin", query = "SELECT u FROM Usuarios u WHERE u.isAdmin = :isAdmin")})
public class Usuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ultimo_nivel_visitado")
    private int ultimoNivelVisitado;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "password")
    private String password;
    @Column(name = "last_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_superuser")
    private boolean isSuperuser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "apellido")
    private String apellido;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_staff")
    private boolean isStaff;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_active")
    private boolean isActive;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_joined")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateJoined;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_student")
    private boolean isStudent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_teacher")
    private boolean isTeacher;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_assistant")
    private boolean isAssistant;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 30)
    @Column(name = "phone")
    private String phone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_logged")
    private boolean isLogged;
    @Basic(optional = false)
    @NotNull
    @Column(name = "avatar")
    private int avatar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "maximo_nivel")
    private int maximoNivel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preguntas_correctas")
    private int preguntasCorrectas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preguntas_erroneas")
    private int preguntasErroneas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preguntas_totales")
    private int preguntasTotales;
    @Size(max = 150)
    @Column(name = "userHash")
    private String userHash;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_admin")
    private boolean isAdmin;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ownerId")
    private Collection<Preguntas> preguntasCollection;
    @OneToMany(mappedBy = "revisorId")
    private Collection<Preguntas> preguntasCollection1;
    @OneToMany(mappedBy = "profesorId")
    private Collection<Usuarios> usuariosCollection;
    @JoinColumn(name = "profesor_id", referencedColumnName = "id")
    @ManyToOne
    private Usuarios profesorId;

    public Usuarios() {
    }

    public Usuarios(Integer id) {
        this.id = id;
    }

    public Usuarios(Integer id, int ultimoNivelVisitado, String password, boolean isSuperuser, String username, String nombre, String apellido, String email, boolean isStaff, boolean isActive, Date dateJoined, boolean isStudent, boolean isTeacher, boolean isAssistant, boolean isLogged, int avatar, int maximoNivel, int preguntasCorrectas, int preguntasErroneas, int preguntasTotales, boolean isAdmin) {
        this.id = id;
        this.ultimoNivelVisitado = ultimoNivelVisitado;
        this.password = password;
        this.isSuperuser = isSuperuser;
        this.username = username;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.isStaff = isStaff;
        this.isActive = isActive;
        this.dateJoined = dateJoined;
        this.isStudent = isStudent;
        this.isTeacher = isTeacher;
        this.isAssistant = isAssistant;
        this.isLogged = isLogged;
        this.avatar = avatar;
        this.maximoNivel = maximoNivel;
        this.preguntasCorrectas = preguntasCorrectas;
        this.preguntasErroneas = preguntasErroneas;
        this.preguntasTotales = preguntasTotales;
        this.isAdmin = isAdmin;
    }
    
    public Usuarios(String username, String nombre, String contrasena, int maximoNivel, int ultimoNivelVisitado, int preguntasTotales, int preguntasCorrectas, int preguntasErroneas, int avatar) {
        this.username = username;
        this.nombre = nombre;
        this.password = contrasena;
        this.maximoNivel = maximoNivel;
        this.ultimoNivelVisitado = ultimoNivelVisitado;
        this.preguntasTotales = preguntasTotales;
        this.preguntasCorrectas = preguntasCorrectas;
        this.preguntasErroneas = preguntasErroneas;
        this.avatar = avatar;
    }

    
    public Integer getUserId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    @XmlTransient
    @JsonIgnore
    public String getContrasena() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @XmlTransient
    @JsonIgnore
    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsSuperuser() {
        return isSuperuser;
    }

    public void setIsSuperuser(boolean isSuperuser) {
        this.isSuperuser = isSuperuser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre() {
        return nombre + " "+ this.apellido;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    @XmlTransient
    @JsonIgnore
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    @XmlTransient
    @JsonIgnore
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsStaff() {
        return isStaff;
    }

    public void setIsStaff(boolean isStaff) {
        this.isStaff = isStaff;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
    @XmlTransient
    @JsonIgnore
    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsStudent() {
        return isStudent;
    }

    public void setIsStudent(boolean isStudent) {
        this.isStudent = isStudent;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsTeacher() {
        return isTeacher;
    }

    public void setIsTeacher(boolean isTeacher) {
        this.isTeacher = isTeacher;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsAssistant() {
        return isAssistant;
    }

    public void setIsAssistant(boolean isAssistant) {
        this.isAssistant = isAssistant;
    }
@XmlTransient
    @JsonIgnore
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsLogged() {
        return isLogged;
    }

    public void setIsLogged(boolean isLogged) {
        this.isLogged = isLogged;
    }
    
    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public int getPreguntas_totales() {
        return preguntasTotales;
    }

    public void setPreguntasTotales(int preguntasTotales) {
        this.preguntasTotales = preguntasTotales;
    }

    public int getPreguntas_correctas() {
        return preguntasCorrectas;
    }

    public void setPreguntasAcertadas(int preguntasAcertadas) {
        this.preguntasCorrectas = preguntasAcertadas;
    }

    public int getPreguntas_erroneas() {
        return preguntasErroneas;
    }

    public void setPreguntasEquivocadas(int preguntasEquivocadas) {
        this.preguntasErroneas = preguntasEquivocadas;
    }

    public int getMaximo_nivel() {
        return maximoNivel;
    }

    public void setMaximoNivelAlcanzado(int maximoNivelAlcanzado) {
        this.maximoNivel = maximoNivelAlcanzado;
    }

    public int getUltimo_nivel_visitado() {
        return ultimoNivelVisitado;
    }

    public void setUltimoNivelVisitado(int ultimoNivelVisitado) {
        this.ultimoNivelVisitado = ultimoNivelVisitado;
    }

    public String getUserHash() {
        return userHash;
    }

    public void setUserHash(String userHash) {
        this.userHash = userHash;
    }
    @XmlTransient
    @JsonIgnore
    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @XmlTransient
    @JsonIgnore
    public Collection<Preguntas> getPreguntasCollection() {
        return preguntasCollection;
    }

    public void setPreguntasCollection(Collection<Preguntas> preguntasCollection) {
        this.preguntasCollection = preguntasCollection;
    }

    @XmlTransient
    @JsonIgnore
    public Collection<Preguntas> getPreguntasCollection1() {
        return preguntasCollection1;
    }

    public void setPreguntasCollection1(Collection<Preguntas> preguntasCollection1) {
        this.preguntasCollection1 = preguntasCollection1;
    }

    @XmlTransient
    @JsonIgnore
    public Collection<Usuarios> getUsuariosCollection() {
        return usuariosCollection;
    }

    public void setUsuariosCollection(Collection<Usuarios> usuariosCollection) {
        this.usuariosCollection = usuariosCollection;
    }
    @XmlTransient
    @JsonIgnore
    public Usuarios getProfesorId() {
        return profesorId;
    }

    public void setProfesorId(Usuarios profesorId) {
        this.profesorId = profesorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuarios)) {
            return false;
        }
        Usuarios other = (Usuarios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "espol.edu.ec.model.Usuarios[ id=" + id + " ]";
    }
    
}
