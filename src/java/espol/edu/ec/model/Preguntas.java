/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import espol.edu.ec.utils.Utils;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
@Table(name = "preguntas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preguntas.findAll", query = "SELECT p FROM Preguntas p")
    , @NamedQuery(name = "Preguntas.findByVecesAcertada", query = "SELECT p FROM Preguntas p WHERE p.vecesAcertada = :vecesAcertada")
    , @NamedQuery(name = "Preguntas.findByVecesFallada", query = "SELECT p FROM Preguntas p WHERE p.vecesFallada = :vecesFallada")
    , @NamedQuery(name = "Preguntas.findById", query = "SELECT p FROM Preguntas p WHERE p.id = :id")
    , @NamedQuery(name = "Preguntas.findByContenido", query = "SELECT p FROM Preguntas p WHERE p.contenido = :contenido")
    , @NamedQuery(name = "Preguntas.findByDificultad", query = "SELECT p FROM Preguntas p WHERE p.dificultad = :dificultad")
    , @NamedQuery(name = "Preguntas.findByFecha", query = "SELECT p FROM Preguntas p WHERE p.fecha = :fecha")
    , @NamedQuery(name = "Preguntas.findByObservacion", query = "SELECT p FROM Preguntas p WHERE p.observacion = :observacion")
    , @NamedQuery(name = "Preguntas.findByImagen", query = "SELECT p FROM Preguntas p WHERE p.imagen = :imagen")
    , @NamedQuery(name = "Preguntas.findByTiempoEnSegundos", query = "SELECT p FROM Preguntas p WHERE p.tiempoEnSegundos = :tiempoEnSegundos")
    , @NamedQuery(name = "Preguntas.findByState", query = "SELECT p FROM Preguntas p WHERE p.state = :state")})
public class Preguntas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "veces_acertada")
    private int vecesAcertada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "veces_fallada")
    private int vecesFallada;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "contenido")
    private String contenido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dificultad")
    private int dificultad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Size(max = 255)
    @Column(name = "observacion")
    private String observacion;
    @Size(max = 200)
    @Column(name = "imagen")
    private String imagen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tiempo_en_segundos")
    private float tiempoEnSegundos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "state")
    private String state;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionId")
    private Collection<Respuestas> respuestasCollection;
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuarios ownerId;
    @JoinColumn(name = "revisor_id", referencedColumnName = "id")
    @ManyToOne
    private Usuarios revisorId;
    @JoinColumn(name = "nivel", referencedColumnName = "nivel")
    @ManyToOne(optional = false)
    private Niveles nivel;

    public Preguntas() {
    }

    public Preguntas(Integer id) {
        this.id = id;
    }

    public Preguntas(Integer id, int vecesAcertada, int vecesFallada, String contenido, int dificultad, Date fecha, float tiempoEnSegundos, String state) {
        this.id = id;
        this.vecesAcertada = vecesAcertada;
        this.vecesFallada = vecesFallada;
        this.contenido = contenido;
        this.dificultad = dificultad;
        this.fecha = fecha;
        this.tiempoEnSegundos = tiempoEnSegundos;
        this.state = state;
    }
    
    public Preguntas(Integer idPregunta, String contenido, float tiempoEnSegundos, int dificultad) {
        this.id = idPregunta;
        this.contenido = contenido;
        this.tiempoEnSegundos = tiempoEnSegundos;
        this.dificultad = dificultad;
    }

    public Integer getIdPregunta() {
        return id;
    }
    
    public void setIdPregunta(Integer idPregunta) {
        this.id = idPregunta;
    }
    
    public String getContenido() {
        Utils utils = new Utils();
        String resultado  = utils.etiquetarTexto(contenido);
        return resultado;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
    
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    
    public float getTiempo_en_segundos() {
        return tiempoEnSegundos;
    }

    public void setTiempoEnSegundos(float tiempoEnSegundos) {
        this.tiempoEnSegundos = tiempoEnSegundos;
    }
    
    public int getDificultad() {
        return dificultad;
    }

    public void setDificultad(int dificultad) {
        this.dificultad = dificultad;
    }
    
    @XmlTransient
    @JsonIgnore
    public int getVecesAcertada() {
        return vecesAcertada;
    }

    public void setVecesAcertada(int vecesAcertada) {
        this.vecesAcertada = vecesAcertada;
    }
    @XmlTransient
    @JsonIgnore
    public int getVecesFallada() {
        return vecesFallada;
    }

    public void setVecesFallada(int vecesFallada) {
        this.vecesFallada = vecesFallada;
    }
    
    
    public Collection<Respuestas> getAnswers() {
        return respuestasCollection;
    }

    public void setRespuestasCollection(Collection<Respuestas> respuestasCollection) {
        this.respuestasCollection = respuestasCollection;
    }
    
    
    
    
    @XmlTransient
    @JsonIgnore
    public Integer getUserId() {
        return id;
    }
    
    @XmlTransient
    @JsonIgnore
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    @XmlTransient
    @JsonIgnore
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

        
    @XmlTransient
    @JsonIgnore
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    
    
    @XmlTransient
    @JsonIgnore
    public Usuarios getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Usuarios ownerId) {
        this.ownerId = ownerId;
    }
    @XmlTransient
    @JsonIgnore
    public Usuarios getRevisorId() {
        return revisorId;
    }

    public void setRevisorId(Usuarios revisorId) {
        this.revisorId = revisorId;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getNivel() {
        return nivel.getNivel();
    }

    public void setNivel(Niveles nivel) {
        this.nivel = nivel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preguntas)) {
            return false;
        }
        Preguntas other = (Preguntas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "espol.edu.ec.model.Preguntas[ id=" + id + " ]";
    }
    
}
