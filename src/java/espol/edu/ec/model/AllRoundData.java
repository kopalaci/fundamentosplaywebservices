/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
public class AllRoundData implements Serializable {

    @Id
    private List<Preguntas> preguntas;

    public AllRoundData(List<Preguntas> preguntas) {
        this.preguntas = preguntas;
    }

    public AllRoundData() {
    }

    public List<Preguntas> getQuestions() {
        return preguntas;
    }

    public void setPreguntas(List<Preguntas> preguntas) {
        this.preguntas = preguntas;
    }
    
}
