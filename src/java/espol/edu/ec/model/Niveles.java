/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
@Table(name = "niveles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Niveles.findAll", query = "SELECT n FROM Niveles n")
    , @NamedQuery(name = "Niveles.findByNivel", query = "SELECT n FROM Niveles n WHERE n.nivel = :nivel")
    , @NamedQuery(name = "Niveles.findByPreguntasTotales", query = "SELECT n FROM Niveles n WHERE n.preguntasTotales = :preguntasTotales")
    , @NamedQuery(name = "Niveles.findByPreguntasAcertadas", query = "SELECT n FROM Niveles n WHERE n.preguntasAcertadas = :preguntasAcertadas")
    , @NamedQuery(name = "Niveles.findByPreguntasErroneas", query = "SELECT n FROM Niveles n WHERE n.preguntasErroneas = :preguntasErroneas")})
public class Niveles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "nivel")
    private Integer nivel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preguntas_totales")
    private int preguntasTotales;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preguntas_acertadas")
    private int preguntasAcertadas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "preguntas_erroneas")
    private int preguntasErroneas;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "niveles")
    private Leaderboard leaderboard;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivel")
    private Collection<Preguntas> preguntasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivel")
    private Collection<FichaCompletacion> fichaCompletacionCollection;

    public Niveles() {
    }

    public Niveles(Integer nivel) {
        this.nivel = nivel;
    }

    public Niveles(Integer nivel, int preguntasTotales, int preguntasAcertadas, int preguntasErroneas) {
        this.nivel = nivel;
        this.preguntasTotales = preguntasTotales;
        this.preguntasAcertadas = preguntasAcertadas;
        this.preguntasErroneas = preguntasErroneas;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public int getPreguntasTotales() {
        return preguntasTotales;
    }

    public void setPreguntasTotales(int preguntasTotales) {
        this.preguntasTotales = preguntasTotales;
    }

    public int getPreguntasAcertadas() {
        return preguntasAcertadas;
    }

    public void setPreguntasAcertadas(int preguntasAcertadas) {
        this.preguntasAcertadas = preguntasAcertadas;
    }

    public int getPreguntasErroneas() {
        return preguntasErroneas;
    }

    public void setPreguntasErroneas(int preguntasErroneas) {
        this.preguntasErroneas = preguntasErroneas;
    }
    @XmlTransient
    @JsonIgnore
    public Leaderboard getLeaderboard() {
        return leaderboard;
    }

    public void setLeaderboard(Leaderboard leaderboard) {
        this.leaderboard = leaderboard;
    }

    @XmlTransient
    @JsonIgnore
    public Collection<Preguntas> getPreguntasCollection() {
        return preguntasCollection;
    }

    public void setPreguntasCollection(Collection<Preguntas> preguntasCollection) {
        this.preguntasCollection = preguntasCollection;
    }

    @XmlTransient
    @JsonIgnore
    public Collection<FichaCompletacion> getFichaCompletacionCollection() {
        return fichaCompletacionCollection;
    }

    public void setFichaCompletacionCollection(Collection<FichaCompletacion> fichaCompletacionCollection) {
        this.fichaCompletacionCollection = fichaCompletacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nivel != null ? nivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Niveles)) {
            return false;
        }
        Niveles other = (Niveles) object;
        if ((this.nivel == null && other.nivel != null) || (this.nivel != null && !this.nivel.equals(other.nivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "espol.edu.ec.model.Niveles[ nivel=" + nivel + " ]";
    }
    
}
