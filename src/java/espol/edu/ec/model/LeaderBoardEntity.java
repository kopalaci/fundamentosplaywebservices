/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
public class LeaderBoardEntity implements Serializable {

    @Id
    private List<Boolean> chall;
    
    private List<Float> times;
    
    private List<String> names;
    
    private List<Integer> avatares;
    
    private List<Integer> dias;

    public LeaderBoardEntity() {
    }

    public LeaderBoardEntity(List<Boolean> chall, List<Float> times, List<String> names, List<Integer> avatares, List<Integer> dias) {
        this.chall = chall;
        this.times = times;
        this.names = names;
        this.avatares = avatares;
        this.dias = dias;
    }

    
    
 
    public List<Boolean> getChall() {
        return chall;
    }

    public void setChall(List<Boolean> chall) {
        this.chall = chall;
    }

    public List<Float> getTimes() {
        return times;
    }

    public void setTimes(List<Float> times) {
        this.times = times;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<Integer> getAvatares() {
        return avatares;
    }

    public void setAvatares(List<Integer> avatares) {
        this.avatares = avatares;
    }

    public List<Integer> getDias() {
        return dias;
    }

    public void setDias(List<Integer> dias) {
        this.dias = dias;
    }
    
    

    
    
    
}
