/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
@Table(name = "ficha_completacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FichaCompletacion.findAll", query = "SELECT f FROM FichaCompletacion f")
    , @NamedQuery(name = "FichaCompletacion.findByFichaId", query = "SELECT f FROM FichaCompletacion f WHERE f.fichaId = :fichaId")
    , @NamedQuery(name = "FichaCompletacion.findByMedallaPrincipiante", query = "SELECT f FROM FichaCompletacion f WHERE f.medallaPrincipiante = :medallaPrincipiante")
    , @NamedQuery(name = "FichaCompletacion.findByMedallaIntermedio", query = "SELECT f FROM FichaCompletacion f WHERE f.medallaIntermedio = :medallaIntermedio")
    , @NamedQuery(name = "FichaCompletacion.findByMedallaExperto", query = "SELECT f FROM FichaCompletacion f WHERE f.medallaExperto = :medallaExperto")
    , @NamedQuery(name = "FichaCompletacion.findByPrincipianteFalladas", query = "SELECT f FROM FichaCompletacion f WHERE f.principianteFalladas = :principianteFalladas")
    , @NamedQuery(name = "FichaCompletacion.findByIntermedioFalladas", query = "SELECT f FROM FichaCompletacion f WHERE f.intermedioFalladas = :intermedioFalladas")
    , @NamedQuery(name = "FichaCompletacion.findByPrincipianteAcertadas", query = "SELECT f FROM FichaCompletacion f WHERE f.principianteAcertadas = :principianteAcertadas")
    , @NamedQuery(name = "FichaCompletacion.findByIntermedioAcertadas", query = "SELECT f FROM FichaCompletacion f WHERE f.intermedioAcertadas = :intermedioAcertadas")
    , @NamedQuery(name = "FichaCompletacion.findByExpertoFalladas", query = "SELECT f FROM FichaCompletacion f WHERE f.expertoFalladas = :expertoFalladas")
    , @NamedQuery(name = "FichaCompletacion.findByExpertoAcertadas", query = "SELECT f FROM FichaCompletacion f WHERE f.expertoAcertadas = :expertoAcertadas")})
public class FichaCompletacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "fichaId")
    private Integer fichaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "medalla_principiante")
    private boolean medallaPrincipiante;
    @Basic(optional = false)
    @NotNull
    @Column(name = "medalla_intermedio")
    private boolean medallaIntermedio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "medalla_experto")
    private boolean medallaExperto;
    @Column(name = "principiante_falladas")
    private Integer principianteFalladas;
    @Column(name = "intermedio_falladas")
    private Integer intermedioFalladas;
    @Column(name = "principiante_acertadas")
    private Integer principianteAcertadas;
    @Column(name = "intermedio_acertadas")
    private Integer intermedioAcertadas;
    @Column(name = "experto_falladas")
    private Integer expertoFalladas;
    @Column(name = "experto_acertadas")
    private Integer expertoAcertadas;
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuarios userId;
    @JoinColumn(name = "nivel", referencedColumnName = "nivel")
    @ManyToOne(optional = false)
    private Niveles nivel;

    public FichaCompletacion() {
    }

    public FichaCompletacion(Integer fichaId) {
        this.fichaId = fichaId;
    }

    public FichaCompletacion(Usuarios user, Niveles nivel) {
        this.nivel = nivel;
        this.userId = user;
        this.medallaPrincipiante = false;
        this.medallaIntermedio = false;
        this.medallaExperto = false;
        this.principianteAcertadas=0; this.principianteFalladas=0;
        this.intermedioAcertadas =0; this.intermedioFalladas=0;
        this.expertoAcertadas =0; this.expertoFalladas =0;
    }

    public FichaCompletacion(Integer fichaId, boolean medallaPrincipiante, boolean medallaIntermedio, boolean medallaExperto) {
        this.fichaId = fichaId;
        this.medallaPrincipiante = medallaPrincipiante;
        this.medallaIntermedio = medallaIntermedio;
        this.medallaExperto = medallaExperto;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getFichaId() {
        return fichaId;
    }

    public void setFichaId(Integer fichaId) {
        this.fichaId = fichaId;
    }

    public boolean getMedalla_principiante()  {
        return medallaPrincipiante;
    }

    public void setMedallaPrincipiante(boolean medallaPrincipiante) {
        this.medallaPrincipiante = medallaPrincipiante;
    }

    public boolean getMedalla_intermedio() {
        return medallaIntermedio;
    }

    public void setMedallaIntermedio(boolean medallaIntermedio) {
        this.medallaIntermedio = medallaIntermedio;
    }

    public boolean getMedalla_experto() {
        return medallaExperto;
    }

    public void setMedallaExperto(boolean medallaExperto) {
        this.medallaExperto = medallaExperto;
    }
    @XmlTransient
    @JsonIgnore
    public Usuarios getUsuarios() {
        return userId;
    }

    public void setUsuarios(Usuarios usuario) {
        this.userId = usuario;
    }
    
    @XmlTransient
    @JsonIgnore
    public Integer getPrincipianteAcertadas() {
        return principianteAcertadas;
    }

    public void setPrincipianteAcertadas(Integer principianteAcertadas) {
        this.principianteAcertadas = principianteAcertadas;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getIntermedioAcertadas() {
        return intermedioAcertadas;
    }

    public void setIntermedioAcertadas(Integer intermedioAcertadas) {
        this.intermedioAcertadas = intermedioAcertadas;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getExpertoAcertadas() {
        return expertoAcertadas;
    }

    public void setExpertoAcertadas(Integer expertoAcertadas) {
        this.expertoAcertadas = expertoAcertadas;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getPrincipanteFalladas() {
        return principianteFalladas;
    }

    public void setPrincipanteFalladas(Integer principanteFalladas) {
        this.principianteFalladas = principanteFalladas;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getIntermedioFalladas() {
        return intermedioFalladas;
    }

    public void setIntermedioFalladas(Integer intermedioFalladas) {
        this.intermedioFalladas = intermedioFalladas;
    }
    @XmlTransient
    @JsonIgnore
    public Integer getExpertoFalladas() {
        return expertoFalladas;
    }

    public void setExpertoFalladas(Integer expertoFalladas) {
        this.expertoFalladas = expertoFalladas;
    }
    
    public Integer getNivel() {
        return nivel.getNivel();
    }

    public void setNivel(Niveles nivel) {
        this.nivel = nivel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fichaId != null ? fichaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichaCompletacion)) {
            return false;
        }
        FichaCompletacion other = (FichaCompletacion) object;
        if ((this.fichaId == null && other.fichaId != null) || (this.fichaId != null && !this.fichaId.equals(other.fichaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "espol.edu.ec.model.FichaCompletacion[ fichaId=" + fichaId + " ]";
    }
    
}
