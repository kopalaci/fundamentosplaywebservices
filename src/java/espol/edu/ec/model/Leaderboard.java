/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
@Table(name = "leaderboard")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Leaderboard.findAll", query = "SELECT l FROM Leaderboard l")
    , @NamedQuery(name = "Leaderboard.findByTiempo", query = "SELECT l FROM Leaderboard l WHERE l.tiempo = :tiempo")
    , @NamedQuery(name = "Leaderboard.findByNivel", query = "SELECT l FROM Leaderboard l WHERE l.nivel = :nivel")
    , @NamedQuery(name = "Leaderboard.findByStartDay", query = "SELECT l FROM Leaderboard l WHERE l.startDay = :startDay")})
public class Leaderboard implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "tiempo")
    private String tiempo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "nivel")
    private Integer nivel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_day")
    @Temporal(TemporalType.DATE)
    private Date startDay;
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuarios userId;
    @JoinColumn(name = "nivel", referencedColumnName = "nivel", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Niveles niveles;

    public Leaderboard() {
    }

    public Leaderboard(Integer nivel) {
        this.nivel = nivel;
    }

    public Leaderboard(Integer nivel, String tiempo) {
        this.nivel = nivel;
        this.tiempo = tiempo;
    }
    
    public Leaderboard(String tiempo, Integer nivel, Usuarios userId) {
        this.tiempo = tiempo;
        this.nivel = nivel;
        this.userId = userId;
    }

    public Leaderboard(String tiempo, Integer nivel, Usuarios userId, Date start_day) {
        this.tiempo = tiempo;
        this.nivel = nivel;
        this.userId = userId;
        this.startDay = start_day;
    }
    
    

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Date getStart_day() {
        return startDay;
    }

    public void setStartDay(Date startDay) {
        this.startDay = startDay;
    }

    public Integer getUserId() {
        return userId.getUserId();
    }

    public void setUserId(Usuarios userId) {
        this.userId = userId;
    }
      @XmlTransient
    @JsonIgnore
    public Niveles getNiveles() {
        return niveles;
    }

    public void setNiveles(Niveles niveles) {
        this.niveles = niveles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nivel != null ? nivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Leaderboard)) {
            return false;
        }
        Leaderboard other = (Leaderboard) object;
        if ((this.nivel == null && other.nivel != null) || (this.nivel != null && !this.nivel.equals(other.nivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "espol.edu.ec.model.Leaderboard[ nivel=" + nivel + " ]";
    }
    
}
