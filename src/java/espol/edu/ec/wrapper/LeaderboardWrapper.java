/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.wrapper;

import espol.edu.ec.model.Leaderboard;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
public class LeaderboardWrapper implements Serializable {

    @Id
    private List<Leaderboard> leaderboard;

    public LeaderboardWrapper(List<Leaderboard> leaderboard) {
        this.leaderboard = leaderboard;
    }

    public LeaderboardWrapper() {
    }

    public List<Leaderboard> getLeaderboard() {
        return leaderboard;
    }

    public void setLeaderboard(List<Leaderboard> leaderboard) {
        this.leaderboard = leaderboard;
    }
    
    
}
