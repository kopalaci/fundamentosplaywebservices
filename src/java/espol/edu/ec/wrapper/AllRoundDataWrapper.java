/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.wrapper;

import espol.edu.ec.model.AllRoundData;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Kevin Omar Palacios Alvarez
 */
@Entity
public class AllRoundDataWrapper implements Serializable {

    @Id
    private List<AllRoundData> allRoundData;

    public AllRoundDataWrapper(List<AllRoundData> allRoundData) {
        this.allRoundData = allRoundData;
    }

    public AllRoundDataWrapper() {
    }

    public List<AllRoundData> getAllRoundData() {
        return allRoundData;
    }

    public void setAllRoundData(List<AllRoundData> allRoundData) {
        this.allRoundData = allRoundData;
    }

    
    
    
}
